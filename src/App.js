import './App.css';
import { Details } from './Components/Details';
import { useState, useCallback } from 'react';

import React from 'react'

export default function App() {
  

  let [data, setData] = useState(
    {
      230493: {
        name: "Boris",
        salary: 1000,
        details: {
          state: "Pennsylvennia",
          zip: 3403249
        }
      },
      230494: {
        name: "Mike",
        salary: 2000,
        details: {
          state: "Ohio",
          zip: 3403249
        }
      },
      230495: {
        name: "Pheonix",
        salary: 3000,
        details: {
          state: "Newyork",
          zip: 3403249,
          coordinates: { lat: 43, lng: 22 }
        }
      },
      230496: {
        name: "Mark",
        salary: 5000,
        details: {
          state: "Ohio",
          zip: 3403222
        }
      },
      230497: {
        name: "Mike",
        salary: 2000,
        details: {
          state: "Cleveland",
          zip: 3403249,
          coordinates: { lat: 34, lng: 21 }
        }
      },
      230498: {
        name: "Shin",
        salary: 2000,
        details: {
          state: "Newyork",
          zip: 3403249,
        }
      }

    }
  )


  // 1. Removing item with id = 230495

  let remove = useCallback((id) => {
    let newData = Object.keys(data).reduce((acc, cur) => {
      if (cur !== id) {
        acc[cur] = data[cur]
      }
      return acc;
    }, {})
    setData(newData)
    console.log("one")
  }, [])
  //remove("230495")


  // 2. Adding a new item with the following details
  // { empId: 399424, name: John, salary: 4000, zip: 434043 }

  let addItem = useCallback((newItem) => {
    let newDAta = Object.keys(data).reduce((acc, cur, ind) => {
      if (ind === 0) {
        acc[newItem.empId] = { name: newItem.name, salary: newItem.salary, details: { state: newItem.state, zip: newItem.zip } }
      }
      acc[cur] = data[cur]
      return acc;
    }, {})
    setData(newDAta)
    console.log("two")

  }, [])
  //addItem({ empId: 399424, name: "John", salary: 4000,state:"AP", zip: 434043 })



  // 3. Change "state" of the employee with id = 230496 to "Maharastra".

  let changeState = useCallback((empId, newState) => {
    let newDAta = Object.keys(data).reduce((acc, cur) => {
      (cur === empId)
        ? acc[cur] = { ...data[cur], details: { ...data[cur].details, state: newState } }
        : acc[cur] = data[cur]

      return acc;
    }, {})
    setData(newDAta)
    console.log("three")
  }, [])

  //changeState("230496","maharastra")


  // 4. Multiply each person's the salary by 5.

  let mul = useCallback((x) => {
    let newData = Object.keys(data).reduce((acc, cur) => {
      acc[cur] = { ...data[cur], details: { ...data[cur].details, }, salary: data[cur].salary * x }
      return acc;
    }, {})
    setData(newData)
    console.log("four")
  }, [])
  //mul()


  // 5. Add a new property "federalTax" with value 10 for all employees from state "Ohio".

  let AddTax = useCallback((State) => {
    let newDAta = Object.keys(data).reduce((acc, cur) => {
      if (data[cur].details.state === State) {
        acc[cur] = { ...data[cur], details: { ...data[cur].details } }
        acc[cur].details.federalTax = 10
      } else {
        acc[cur] = data[cur]
      }
      return acc
    }, {})
    setData(newDAta)
    console.log("five")
  }, [])
  //AddTax("Ohio")


  // 6. Remove lat property for all persons.

  let removeLat = useCallback(() => {
    let newDAta = Object.keys(data).reduce((acc, cur) => {
      if (data[cur].details.coordinates) {
        let { lat, ...rest } = data[cur].details.coordinates

        acc[cur] = { ...data[cur], details: { ...data[cur].details, coordinates: { ...rest } } }
      } else {
        acc[cur] = data[cur]
      }
      return acc;
    }, {})
    setData(newDAta)
    console.log("six")
  }, [])
  //removeLat()


  // 7. Filter and display data based on states. 
  //         [use html select to choose a state and display data for only that state]

  let selectState = useCallback((STATE) => {
    let newData = Object.keys(data).reduce((acc, cur) => {
      if (data[cur].details.state === STATE) {
        acc[cur] = data[cur]
      }
      return acc;
    }, {})
    setData(newData)
    console.log("seven")
  }, [])


  // sorting 
  let sort = useCallback((data) => {
    console.log("sort")
    return Object.keys(data).sort((a, b) => {
      if (data[a].name !== data[b].name) {
        return (data[a].name > data[b].name) ? 1 : -1;
      } else if (data[a].details.state !== data[b].details.state) {
        return (data[a].details.state > data[b].details.state) ? 1 : -1;
      } else {
        return (data[a].salary > data[b].salary) ? -1 : 1;
      }
    })

  }, [])


  return <div>
    <div className='select'> <br />
      <select name="state" onChange={(e) => selectState(e.target.value)}>
        <option >Select State</option>
        <option value="Newyork">Newyork</option>
        <option value="Cleveland">Cleveland</option>
        <option value="Ohio">Ohio</option>
        <option value="Pennsylvennia">Pennsylvennia</option>
      </select></div>
    <div className='emps'>
      <ul className='data'>{

        sort(data).map((empId) => {
          return <li key={empId}>{empId}:
            <ul>
              <li>name: {data[empId].name}</li>
              <li>salary: {data[empId].salary}</li>
              <li>Details:
                <Details details={data[empId].details} /><hr></hr>
              </li>
            </ul>
          </li>
        })

      }
      </ul>
      <ol className='btns'>
        <li><button onClick={() => remove("230495")}>Removing item with id = 230495</button></li>
        <li><button onClick={() => addItem({ empId: 399424, name: "John", salary: 4000, state: "AP", zip: 434043 })}>Adding a new item </button></li>
        <li><button onClick={() => changeState("230496", "maharastra")}>Change "state" of id = 230496</button></li>
        <li><button onClick={() => mul(5)}> Multiply salary by 5.</button></li>
        <li><button onClick={() => AddTax("Ohio")}>Add a new property "federalTax" employees from "Ohio".</button></li>
        <li><button onClick={() => removeLat()}> Remove lat property</button></li>
      </ol>

    </div></div>


}


