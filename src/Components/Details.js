import React from 'react'
const Coordinates = ({coordinates}) => {
    let {lat, lng} = coordinates;
    return <ul>
     {lat?<li>Lat:{lat}</li>:<></>}
    {lng?<li>Lat:{lng}</li>:<></>}
    </ul>
}
export const Details = ({details}) => {
    let {state,zip,coordinates, federalTax}=details;
  return (<ul> 
    <li> State : {state}</li>
    <li>Zip : {zip} </li>
    {federalTax ? <li>federalTax : {federalTax} </li> : <></>}
    {coordinates ? <li>coordinates: <Coordinates  coordinates={coordinates}/></li>:<></>} 
    </ul>
  )
}
